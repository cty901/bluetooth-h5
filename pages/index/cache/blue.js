//网上的连接蓝牙打印机代码

let device = null,
	BAdapter = null,
	BluetoothAdapter = null,
	uuid = null,
	main = null,
	BleDevice = null,
	UUID = null,
	bluetoothSocket = null;

//address=""搜索蓝牙//address=设备mac地址，自动配对给出mac地址的设备
export function searchDevices(address, backBtn, backList) {
	//注册类
	let main = plus.android.runtimeMainActivity();
	let IntentFilter = plus.android.importClass('android.content.IntentFilter');
	let BluetoothAdapter = plus.android.importClass("android.bluetooth.BluetoothAdapter");
	let BluetoothDevice = plus.android.importClass("android.bluetooth.BluetoothDevice");
	let BAdapter = BluetoothAdapter.getDefaultAdapter();
	console.log("开始搜索设备");
	let filter = new IntentFilter();
	let bdevice = new BluetoothDevice();
	let on = null;
	let un = null;
	backBtn({
		text: "正在搜索请稍候",
		disabled: true
	});
	BAdapter.startDiscovery(); //开启搜索
	let receiver;
	receiver = plus.android.implements('io.dcloud.android.content.BroadcastReceiver', {
		onReceive: function(context, intent) { //实现onReceiver回调函数
			plus.android.importClass(intent); //通过intent实例引入intent类，方便以后的‘.’操作
			console.log(intent.getAction()); //获取action
			if (intent.getAction() == "android.bluetooth.adapter.action.DISCOVERY_FINISHED") {
				main.unregisterReceiver(receiver); //取消监听
				backBtn({
					text: '搜索设备',
					disabled: false
				})
				console.log("搜索结束")
			} else {
				BleDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				backList({
					address: BleDevice.getAddress(),
					name: BleDevice.getName()
				})

				//判断是否配对
				if (BleDevice.getBondState() == bdevice.BOND_NONE) {
					console.log("未配对蓝牙设备：" + BleDevice.getName() + '    ' + BleDevice.getAddress());
					//参数如果跟取得的mac地址一样就配对
					if (address == BleDevice.getAddress()) {
						if (BleDevice.createBond()) { //配对命令.createBond()
							console.log("配对成功");
							backList({
								address: BleDevice.getAddress(),
								name: BleDevice.getName()
							})
						}
					} else {
						if (BleDevice.getName() != on) { //判断防止重复添加
							backList({
								address: BleDevice.getAddress(),
								name: BleDevice.getName()
							})
						}
					}
				} else {
					if (BleDevice.getName() != un) { //判断防止重复添加
						backList({
							address: BleDevice.getAddress(),
							name: BleDevice.getName()
						})
					}
				}
			}
		}
	});

	filter.addAction(bdevice.ACTION_FOUND);
	filter.addAction(BAdapter.ACTION_DISCOVERY_STARTED);
	filter.addAction(BAdapter.ACTION_DISCOVERY_FINISHED);
	filter.addAction(BAdapter.ACTION_STATE_CHANGED);

	main.registerReceiver(receiver, filter); //注册监听
}

export function print(mac_address) {
	if (!mac_address) {
		mui.toast('请选择蓝牙打印机');
		return;
	}

	main = plus.android.runtimeMainActivity();
	BluetoothAdapter = plus.android.importClass("android.bluetooth.BluetoothAdapter");
	UUID = plus.android.importClass("java.util.UUID");
	uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	BAdapter = BluetoothAdapter.getDefaultAdapter();
	device = BAdapter.getRemoteDevice(mac_address);
	plus.android.importClass(device);
	bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(uuid);
	plus.android.importClass(bluetoothSocket);

	if (!bluetoothSocket.isConnected()) {
		console.log('检测到设备未连接，尝试连接....');
		bluetoothSocket.connect();
	}

	console.log('设备已连接');

	if (bluetoothSocket.isConnected()) {
		let outputStream = bluetoothSocket.getOutputStream();
		plus.android.importClass(outputStream);
		let string = "打印测试\r\n";
		let bytes = plus.android.invoke(string, 'getBytes', 'gbk');
		outputStream.write(bytes);
		outputStream.flush();
		device = null //这里关键
		bluetoothSocket.close(); //必须关闭蓝牙连接否则意外断开的话打印错误

	}

}
