//改写项目中的代码
export class BlueToothPlus {
  constructor() {
    this.openBluetoothAdapter()
    this.listenerDeviceFound()
    this.searchListBack = null//搜索结果回调
    this.searchList = []//搜索到的设备列表
  }

  // 打开蓝牙模块
  openBluetoothAdapter() {
    plus.bluetooth.openBluetoothAdapter({
      success: function (e) {
        console.log('open success: ' + JSON.stringify(e));
      },
      fail: function (e) {
        console.log('open failed: ' + JSON.stringify(e));
      }
    });
  }

  // 监听发现新设备
  listenerDeviceFound() {
    plus.bluetooth.onBluetoothDeviceFound((e) => {
      let devices = e.devices;
      // console.log('device found: ' + JSON.stringify(e));
      // console.log('device found: ' + JSON.stringify(devices));
      this.searchList = this.searchList.concat(devices)
      this.searchListBack(this.searchList)
    });
  }

  // 开始搜索蓝牙
  startBluetoothDiscovery({searchListBack}) {
    this.searchListBack = searchListBack
    plus.bluetooth.openBluetoothAdapter({
      success: function (e) {
        console.log('open success: ' + JSON.stringify(e));
        plus.bluetooth.startBluetoothDevicesDiscovery({
          success: function (e) {
            console.log('start discovery success: ' + JSON.stringify(e));
          },
          fail: function (e) {
            console.log('start discovery failed: ' + JSON.stringify(e));
          }
        });
      },
      fail: function (e) {
        console.log('open failed: ' + JSON.stringify(e));
      }
    });
  }

  // 连接蓝牙设备
  createConnection({deviceId}) {
    plus.bluetooth.createBLEConnection({
      deviceId: deviceId,
      success: (e) => {
        console.log('create connection success: ' + JSON.stringify(e));
        this.getConnectedDevices()
      },
      fail: (e) => {
        console.log('create connection failed: ' + JSON.stringify(e));
      }
    });
  }

// 获取已连接的蓝牙设备
  getConnectedDevices() {
    plus.bluetooth.getConnectedBluetoothDevices({
      success: function (e) {
        var devices = e.devices;
        console.log('connected devices success: ' + e.length);
        for (var i in devices) {
          console.log(i + ': ' + JSON.stringify(devices[i]));
        }
      },
      fail: function (e) {
        console.log('connected devices failed: ' + JSON.stringify(e));
      }
    });
  }
}
