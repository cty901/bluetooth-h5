export class Request {
  constructor() {
    this.recycleNum = 0
    this.yhdabh = "010100006556"
    this.deviceId = '54:6C:0E:35:78:18'
    this.msgUrl = "http://10.126.29.164:8866/hlwyy/business-mdej/zdcz/sendMessage"
    this.header = {
      'Authorization': 'Basic YWljcF9hcHA6UVd4aFpHUnBianB2Y0dWdUlITmxjMkZ0WlE=',
      "hlwyy-Nonce": "KvFT01IKVumwLkOuB1wCf6BRKK5p2lP0",
      "hlwyy-Token": "2d92e30064ff42d7a19b2976793e8b6f",
      'X-Hci-access-Token': 'YvlJ6GDk4QuUovjN6a70fc8a6100d498',
    }
  }

  showToast(msg) {
    uni.showToast({title: msg})
  }

  async getData(url, data) {
    console.log("getData===params", data)
    let result = await uni.request({url, data, header: this.header})
    console.log("getData===result", result)
    let {data: dataArr} = result[1], {data: resData} = dataArr
    console.log("getData===resData", resData)
    let {sbyy} = resData
    if (sbyy) this.showToast(sbyy)
    return resData
  }

  async firstRequest() {
    let data = {yhdabh: this.yhdabh, ywbsm: '500601'}
    let {bwnr} = await this.getData(this.msgUrl, data)
    return bwnr
  }

  async circleRequest(values) {
    let data = {yhdabh: this.yhdabh, ywbsm: '000101', bwnr: values}
    let result = await this.getData(this.msgUrl, data)
    this.recycleNum += 1
    console.log(`<<<<<第${this.recycleNum}次携带报文内容请求报文内容`, result.bwnr)
    return result
  }

}
